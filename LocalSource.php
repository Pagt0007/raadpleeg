<?php

class LocalSource extends Source
{

    private $target_path = "uploads/";


    function getResource(String $resourceName)
    {
        return file_get_contents($this->target_path.$resourceName.".txt");
    }

    function resourceExists()
    {
        // TODO: Implement resourceExists() method.
    }

    function getFileType()
    {
        // TODO: Implement getFileType() method.
    }

    function createResource($resourceName)
    {
        fopen($this->target_path. $resourceName . ".txt", "w");
    }

    function editResource($textToReplace, $resourceName)
    {
        $file = fopen($this->target_path.$resourceName.".txt","w");
        fwrite($file,$textToReplace);
    }

    function uploadResource()
    {
        // TODO: Implement uploadResource() method.
    }
}