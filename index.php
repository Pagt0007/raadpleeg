<?php
session_start();
include("Source.php");
$test = new LocalSource();
?>

    <!DOCTYPE html>
    <html>
    <body>

    <form method="post">
        Create a Text File
        <input type="text" name="fileName" id="fileName" placeholder="File naam">
        <input type="submit" value="Create text file" name="submit">
    </form>
    <form method="post">
        Open a file
        <input type="text" name="fileToOpen" id="fileToOpen" placeholder="File naam">
        <input type="submit" value="Open Text File" name="submit">
    </form>
    </br>
    <form method="post">
        Select image to upload:
        <input type="file" name="textToUpload" id="textToUpload">
        <input type="submit" value="Upload Text file" name="submit">
    </form>
    <br>
    <form method="post" id="editForm">
                <textarea rows="4" cols="50" class="Input text" name="editText" required><?php
                    if (isset($_POST['fileToOpen']))
                    {
                        $_SESSION['fileToOpen'] = $_POST['fileToOpen'];
                        echo $test->getResource($_POST['fileToOpen']);
                    }
                    else if (isset($_POST['editText']))
                    {
                        $test->editResource($_POST['editText'], $_SESSION['fileToOpen']);
                        echo $_POST['editText'];
                    }
                    else
                    {
                        echo "Text to edit";
                    }
                    ?></textarea>
        <input type="submit" value="Save Edits" name="submit">
    </form>
    </body>
    </html>

<?php

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if (isset($_POST['fileName'])) {
        $test->createResource($_POST['fileName']);
    }

}