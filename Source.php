<?php
include ("LocalSource.php");
abstract class Source
{

    /**
     * @param String $resourceName Name of the Resource
     * @return mixed Resource defined previously
     */
    abstract function getResource(String $resourceName);

    /**
     * @return mixed boolean, true if the resource exists, false if not
     */
    abstract function resourceExists();

    /**
     * @return String format of the file type "txt" or "gif" for example.
     */
    abstract function getFileType();

    /**
     * @param $textToReplace String to write into the file
     * @param $resourceName Name of the resource
     * @return mixed null
     */
    abstract function editResource($textToReplace, $resourceName);

    /**
     * @param $resourceName Name of the resource you want to create
     * @return mixed null
     */
    abstract function createResource($resourceName);

    /**
     * @return mixed
     */
    abstract function uploadResource();

}

